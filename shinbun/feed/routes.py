from flask import Blueprint, request
from shinbun.feed.service import FeedService


feeds = Blueprint('feeds', __name__, url_prefix='/feeds')


@feeds.route('/', methods=['GET'])
def all():
    res = FeedService.get_all()
    return res.body, res.status, res.headers


@feeds.route('/', methods=['POST'])
def create():
    payload = request.get_json()
    res = FeedService.create(data=payload)
    return res.body, res.status, res.headers


@feeds.route('/<int:feed_id>', methods=['GET'])
def get_feed(feed_id):
    res = FeedService.get_one(feed_id=feed_id)
    return res.body, res.status, res.headers


@feeds.route('/<int:feed_id>', methods=['PUT'])
def update(feed_id):
    payload = request.get_json()
    res = FeedService.update(feed_id=feed_id, new_data=payload)
    return res.body, res.status, res.headers


@feeds.route('/<int:feed_id>', methods=['DELETE'])
def delete(feed_id):
    res = FeedService.delete(feed_id=feed_id)
    return res.body, res.status, res.headers


@feeds.route('/<int:feed_id>/articles', methods=['GET'])
def get_feed_articles(feed_id):
    res = FeedService.get_articles(feed_id=feed_id)
    return res.body, res.status, res.headers
