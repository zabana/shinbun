from shinbun.models import Feed, Article
from shinbun.http.response import Response
from shinbun.exceptions import (
    ObjectNotFoundError,
    MissingFieldsError,
    UniqueConstraintError
)
# from shinbun.exceptions.not_found import ObjectNotFoundError
# from shinbun.exceptions.missing_fields import MissingFieldsError
# from shinbun.exceptions.unique_constraint import UniqueConstraintError


class FeedService(object):

    @classmethod
    def get_all(cls):
        all_feeds = Feed.get_all()
        return Response.ok(data=all_feeds)

    @classmethod
    def get_one(cls, feed_id=None):
        try:
            feed = Feed.get(id=feed_id)
        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))
        else:
            return Response.ok(data=feed)

    @classmethod
    def get_articles(cls, feed_id=None):
        try:
            feed = Feed.get(id=feed_id)
        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))
        else:
            articles = Article.query.filter_by(feed_id=feed['id']).all()
            articles = [Article.to_dict(article) for article in articles]
            return Response.ok(data=articles)

    @classmethod
    def create(cls, data={}):

        try:
            feed = Feed.create(data=data)

        except MissingFieldsError as exc:
            return Response.bad_request(
                reason='missing fields',
                message=str(exc)
            )

        except UniqueConstraintError as exc:
            return Response.conflict(message=str(exc))

        else:
            location = '/feeds/{}'.format(feed['id'])
            return Response.created(data=feed, headers={'location': location})

    @classmethod
    def update(cls, feed_id=None, new_data={}):

        try:
            feed = Feed.update(id=feed_id, data=new_data)

        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))

        except UniqueConstraintError as exc:
            return Response.conflict(message=str(exc))

        else:
            return Response.ok(data=feed)

    @classmethod
    def delete(cls, feed_id=None):
        try:
            Feed.delete(id=feed_id)
        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))
        else:
            return Response.deleted()
