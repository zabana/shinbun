class ObjectNotFoundError(Exception):

    """Raised when no object matching the given id can be found in the DB"""

    def __init__(self, id):
        self.id = id
        default_message = 'No object matching id {} could be found in the db'
        super().__init__(default_message.format(self.id))
