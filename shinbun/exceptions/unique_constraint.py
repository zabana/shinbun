class UniqueConstraintError(Exception):

    """Raised when a unique constraint is triggered"""

    def __init__(self, value):
        self.value = value
        default_message = 'The following field already exists: {}'
        super().__init__(default_message.format(self.value))
