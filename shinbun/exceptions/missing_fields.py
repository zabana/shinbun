class MissingFieldsError(Exception):

    """Raised when a not-null value constraint is triggered"""

    def __init__(self, fields):
        self.fields = fields
        default_message = 'One or more of the following fields are missing: {}'
        super().__init__(default_message.format(self.fields))
