import feedparser


def parse_url(url):
    """
    Takes a URL and returns a parsed feed to be processed later on.
    This function will raise an Exception for three kinds of errors:
    - If there's a bad request
    - If the feed is not found
    - If the request is incorrectly formatted
    """
    feed = feedparser.parse(url)

    try:
        status = feed.status
    except AttributeError:
        raise Exception("An error occured. \
              Please check that the feed URL exists and is well formed")
    else:
        if status == 400:
            raise Exception("Feed returned a status of 400")
        if status == 404:
            raise Exception("Feed not found. \
                            Make sure it exists by checking the URL")
        return feed


def get_source(feed):
    """
    Extracts information about a given source feed
    Namely the title, link and icon returned in a dict as:
    ('title', 'source_url', 'icon')
    """
    f = feed.feed
    return {
        "title": f['title'],
        "source_url": f['link'],
        "favicon_url": f['icon']
    }


def get_articles(feed):
    """
    Extracts article (meta)data
    Namely title, published and link, returned in a dict as:
    ('title', 'published_date', 'url')
    """

    articles = []
    for article in feed.entries:
        article_data = {
            "title": article['title'],
            "published_date":  article['published'],
            "url": article['link']
        }
        articles.append(article_data)

    return articles
