from threading import Thread
from shinbun.parser.parser import parse_url


class ShinbunParser(Thread):

    """
    ShinbunParser inherits from the Thread class and overrides its
    `run` method to allow concurrent feed parsing.
    """

    def __init__(self, queue):
        Thread.__init__(self)
        self.queue = queue

    def run(self):
        while True:
            feed_url = self.queue.get()
            try:
                print("Getting feed info for {}".format(feed_url))
                parse_url(feed_url)
            finally:
                self.queue.task_done()
