from flask import Blueprint, request
from shinbun.article.service import ArticleService


articles = Blueprint('articles', __name__, url_prefix='/articles')


@articles.route('/', methods=['GET'])
def all():
    res = ArticleService.get_all()
    return res.body, res.status, res.headers


@articles.route('/', methods=['POST'])
def create():
    payload = request.get_json()
    res = ArticleService.create(data=payload)
    return res.body, res.status, res.headers


@articles.route('/<int:article_id>', methods=['GET'])
def get_feed(article_id):
    res = ArticleService.get_one(article_id=article_id)
    return res.body, res.status, res.headers


@articles.route('/<int:article_id>', methods=['PUT'])
def update(article_id):
    payload = request.get_json()
    res = ArticleService.update(article_id=article_id, new_data=payload)
    return res.body, res.status, res.headers


@articles.route('/<int:article_id>', methods=['DELETE'])
def delete(article_id):
    res = ArticleService.delete(article_id=article_id)
    return res.body, res.status, res.headers
