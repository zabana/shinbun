from shinbun.models import Article
from shinbun.http.response import Response
from shinbun.exceptions import (
    ObjectNotFoundError,
    MissingFieldsError,
    UniqueConstraintError
)


class ArticleService(object):

    @classmethod
    def get_all(cls):
        articles = Article.get_all()
        return Response.ok(data=articles)

    @classmethod
    def get_one(cls, article_id=None):
        try:
            article = Article.get(id=article_id)
        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))
        else:
            return Response.ok(data=article)

    @classmethod
    def create(cls, data={}):

        try:
            article = Article.create(data=data)

        except MissingFieldsError as exc:
            return Response.bad_request(
                reason='missing fields',
                message=str(exc)
            )

        except UniqueConstraintError as exc:
            return Response.conflict(message=str(exc))

        else:
            location = '/articles/{}'.format(article['id'])
            return Response.created(
                data=article,
                headers={'location': location}
            )

    @classmethod
    def update(cls, article_id=None, new_data={}):

        try:
            article = Article.update(id=article_id, data=new_data)

        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))

        except UniqueConstraintError as exc:
            return Response.conflict(message=str(exc))

        else:
            return Response.ok(data=article)

    @classmethod
    def delete(cls, article_id=None):
        try:
            Article.delete(id=article_id)
        except ObjectNotFoundError as exc:
            return Response.not_found(message=str(exc))
        else:
            return Response.deleted()
