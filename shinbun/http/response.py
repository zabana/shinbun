from collections import namedtuple
from flask import jsonify

ServerResponse = namedtuple('ServerResponse', ['status', 'body', 'headers'])


class Response(object):

    @classmethod
    def ok(cls, data={}, headers={}):
        """HTTP 200 OK"""
        status = 200
        res = {
            'meta': {'type': 'success', 'status': status},
            'data': data
        }
        return cls._new_response(status_code=status, body=res, headers=headers)

    @classmethod
    def created(cls, data={}, headers={}):
        """HTTP 201 CREATED"""
        status = 201
        res = {
            'meta': {
                'type': 'success',
                'status': status,
                'message': 'created'
            },
            'data': data
        }
        return cls._new_response(status_code=status, body=res, headers=headers)

    @classmethod
    def deleted(cls, headers={}):
        """HTTP 204 DELETED"""
        status = 204
        res = {
            'meta': {'type': 'success', 'status': status}
        }
        return cls._new_response(status_code=status, body=res, headers={})

    @classmethod
    def bad_request(cls, reason='bad request', message=None):
        """HTTP 400 BAD REQUEST"""
        status = 400
        res = {
            'meta': {
                'type': 'error',
                'status': status,
                'reason': reason
            },
            'message': message
        }
        return cls._new_response(status_code=status, body=res)

    @classmethod
    def unauthorized(cls):
        """HTTP 401 UNAUTHORIZED"""
        status = 401
        res = {
            'meta': {
                'type': 'error',
                'status': status,
                'reason': 'unauthorized'
            },
            'message': 'You are not authorized to access this resource'
        }
        return cls._new_response(status_code=status, body=res, headers={})

    @classmethod
    def forbidden(cls):
        """HTTP 403 FORBIDDEN"""
        status = 403
        res = {
            'meta': {
                'type': 'error',
                'status': status,
                'reason': 'forbidden'
            },
            'message': "You are not allowed to access this resource"
        }
        return cls._new_response(status_code=status, body=res)

    @classmethod
    def not_found(cls, message=None):
        """HTTP 404 NOT FOUND"""
        status = 404
        res = {
            'meta': {
                'type': 'error',
                'status': status,
                'reason': 'not found'
            },
            'message': message
        }
        return cls._new_response(status_code=status, body=res)

    @classmethod
    def conflict(cls, message=None):
        """HTTP 409 CONFLICT"""
        status = 409
        res = {
            'meta': {
                'type': 'error',
                'status': status,
                'reason': 'conflict'
            },
            'message': message
        }
        return cls._new_response(status_code=status, body=res)

    @classmethod
    def server_error(cls):
        """HTTP 500 INTERNAL SERVER ERROR"""
        status = 500
        res = {
            'meta': {
                'type': 'error',
                'status': status,
                'reason': 'internal server error'
            },
        }
        return cls._new_response(status_code=status, body=res)

    @classmethod
    def _new_response(cls, status_code=None, body={}, headers={}):
        return ServerResponse(status_code, jsonify(body), headers)
