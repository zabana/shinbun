from datetime import datetime as dt
from shinbun.db import db
from shinbun.models.base import Base


class Article(db.Model, Base):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(255), nullable=False)
    rss_feed_url = db.Column(db.String(255), nullable=False)
    guid = db.Column(db.String(255), nullable=False)
    is_read = db.Column(db.Boolean, nullable=False, default=False)
    date_published = db.Column(db.DateTime, nullable=False)
    date_added = db.Column(db.DateTime, nullable=False, default=dt.now())
    feed_id = db.Column(db.Integer, db.ForeignKey('feed.id'), nullable=False)

    __table_args__ = (
        db.UniqueConstraint('feed_id', 'guid', name='unique_feed_guid_pair'),
    )

    def __repr__(self):
        return "{}".format(self.title)
