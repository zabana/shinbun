from shinbun.db import db
from shinbun.exceptions.not_found import ObjectNotFoundError
from shinbun.exceptions.missing_fields import MissingFieldsError
from shinbun.exceptions.unique_constraint import UniqueConstraintError
from sqlalchemy.exc import IntegrityError


class Base(object):

    @classmethod
    def create(cls, data={}):
        obj = cls(**data)
        db.session.add(obj)
        try:
            db.session.commit()
        except IntegrityError as e:
            cls._handle_exception(e)
        else:
            return cls.to_dict(obj)

    @classmethod
    def update(cls, id=None, data=None):
        obj = cls.query.get(id)
        if obj is None:
            raise ObjectNotFoundError(id=id)

        for key, value in data.items():
            setattr(obj, key, value)

        try:
            db.session.commit()
        except IntegrityError as e:
            cls._handle_exception(e)
        else:
            return cls.to_dict(obj)

    @classmethod
    def delete(cls, id=None):
        obj = cls.query.get(id)
        if obj is None:
            raise ObjectNotFoundError(id=id)
        db.session.delete(obj)
        db.session.commit()

    @classmethod
    def get(cls, id=None):
        obj = cls.query.get(id)
        if obj is None:
            raise ObjectNotFoundError(id=id)
        return cls.to_dict(obj)

    @classmethod
    def get_all(cls):
        return [cls.to_dict(obj) for obj in cls.query.all()]

    @classmethod
    def _handle_exception(cls, e):
        error = str(e.__cause__)
        if "unique" in error:
            field = error.split("DETAIL: ")[1].split("=")[0].split(" ")[2]
            raise UniqueConstraintError(field)
        elif "not-null" in error:
            missing_fields = cls._get_missing_fields(e.__dict__['params'])
            raise MissingFieldsError(missing_fields)
        else:
            raise Exception('An unknown error happened server side')

    @classmethod
    def to_dict(cls, obj):
        return dict((col, getattr(obj, col)) for col in obj.__table__.columns.keys())  # noqa F821

    @classmethod
    def _get_missing_fields(cls, fields):
        return [k for k, v in fields.items() if fields[k] is None]
