from datetime import datetime as dt
from shinbun.db import db
from shinbun.models.base import Base
from shinbun.models.article import Article  # noqa: F401


class Feed(db.Model, Base):

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), nullable=False, unique=True)
    subtitle = db.Column(db.String(200), nullable=True)
    site_url = db.Column(db.String(200), nullable=False)
    feed_url = db.Column(db.String(200), nullable=False, unique=True)
    date_added = db.Column(db.DateTime, nullable=False, default=dt.now())
    last_scraped = db.Column(db.DateTime, nullable=False, default=dt.now())
    articles = db.relationship('Article', cascade='all, delete-orphan', backref='feed', lazy=True)  # noqa E501

    def __repr__(self):
        return "Feed: {}".format(self.title)
