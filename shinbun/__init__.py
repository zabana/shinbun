import sys
from flask import Flask
from shinbun.db import db
from utils.helpers import register_cli_commands, register_blueprints
from flask_migrate import Migrate

CONFIG = {
    "prod": "shinbun.config.ProductionConfig",
    "staging": "shinbun.config.StagingConfig",
    "testing": "shinbun.config.TestingConfig",
    "development": "shinbun.config.Config"
}


def create_app(env='development'):
    app = Flask(__name__)
    app.config.from_object(CONFIG[env])
    app.url_map.strict_slashes = False
    db.init_app(app)
    with app.app_context():
        try:
            db.create_all()
        except AttributeError as e:
            sys.exit("DB ERROR {}".format(e))
    Migrate(app, db)
    register_blueprints(app)
    register_cli_commands(app)
    return app
