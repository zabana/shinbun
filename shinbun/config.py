import os

BLUEPRINT_BUNDLES = [
    'shinbun.feed.routes',
    'shinbun.article.routes',
    # 'shinbun.auth.routes',
]


class Config(object):
    """
        Main configuration object for the Shibun REST API.
    """
    DEBUG = True
    TESTING = False
    ENV = 'development'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    SQLALCHEMY_DATABASE_URI = os.environ.get('SHINBUN_DB_URI')


class StagingConfig(Config):
    """
        Staging configuration object for the Shibun REST API.
    """
    ENV = 'staging'
    DEBUG = False


class ProductionConfig(Config):
    """
        Production configuration object for the Shinbun REST API.
    """
    ENV = 'production'
    DEBUG = False


class TestingConfig(Config):
    """
        Testing configuration object for the Shinbun REST API.
    """
    SQLALCHEMY_DATABASE_URI = os.environ.get('SHINBUN_TEST_DB_URI')
    ENV = 'testing'
    DEBUG = True
    TESTING = True
