import os
from shinbun import create_app

config_env = os.environ.get('SHINBUN_CONFIG')
app = create_app(env=config_env)
