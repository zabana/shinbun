FROM python:3.6-slim

LABEL maintainer "Karim Cheurfi <karim.cheurfi@gmail.com>"

WORKDIR /app

COPY requirements.txt .

RUN apt-get update \
    && apt-get install -y build-essential \
    && pip install --no-cache-dir -r requirements.txt \
    && apt-get remove -y build-essential \
    && apt-get purge -y --auto-remove

COPY . /app

EXPOSE 5000
