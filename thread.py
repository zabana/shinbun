from queue import Queue
from shinbun.parser.worker import ShinbunParser
import time


links = [
    "http://rss.cnn.com/rss/cnn_topstories.rss",
    "http://feeds.nytimes.com/nyt/rss/HomePage",
    "http://www.washingtonpost.com/rss/",
    "http://rssfeeds.usatoday.com/usatoday-NewsTopStories",
    "http://www.npr.org/rss/rss.php?id=1001",
    "http://feeds.reuters.com/reuters/topNews",
    "http://newsrss.bbc.co.uk/rss/newsonline_world_edition/americas/rss.xml",
    "http://www.sltrib.com/rss/feed/?sec=/News/Utah/&level=1",
    "http://www.deseretnews.com/site/rss",
    "http://www.ksl.com/xml/148.rss",
    "http://www.utah.gov/whatsnew/rss.xml",
    "http://rssfeeds.thespectrum.com/stgeorge/news",
    "http://feeds.sciencedaily.com/sciencedaily",
    "http://feeds.nature.com/nature/rss/current",
    "http://www.nasa.gov/rss/image_of_the_day.rss",
    "http://feeds.wired.com/wired/index",
    "http://feeds.nytimes.com/nyt/rss/Technology",
    "http://feeds1.nytimes.com/nyt/rss/Sports",
    "http://feeds.feedburner.com/TheDailyPuppy",
]


def main():
    # Create a queue
    start = time.time()
    print("Creating queue")
    queue = Queue()

    # create 8 worker threads
    print("Creating worker threads")
    for i in range(8):
        worker = ShinbunParser(queue)
        worker.daemon = True
        worker.start()

    print("Adding feeds to the queue")
    for link in links:
        queue.put(link)

    queue.join()
    print("finished in {}".format(time.time() - start))


if __name__ == "__main__":
    main()
