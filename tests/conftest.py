import pytest
from shinbun import create_app, db as _db
from commands.init_data import _init_data

"""
Used the following resources to create these fixtures.
https://gitlab.com/patkennedy79/flask_user_management_example/blob/master/tests/conftest.py
https://github.com/sunscrapers/flask-boilerplate/blob/master/database.py
https://xvrdm.github.io/2017/07/03/testing-flask-sqlalchemy-database-with-pytest/
"""


@pytest.fixture(scope='module')
def command():
    from click.core import Command
    return Command(name="My Command")


@pytest.fixture(scope='module')
def group():
    from flask.cli import AppGroup
    return AppGroup("My Group")


@pytest.fixture(scope='module')
def _blueprint():
    from flask.blueprints import Blueprint
    return Blueprint('my_blueprint', __name__)


@pytest.fixture(scope='module')
def app():
    app = create_app(env='testing')
    ctx = app.app_context()
    ctx.push()
    yield app
    ctx.pop()


@pytest.fixture(scope='module')
def db(app):
    _db.init_app(app)
    _db.create_all()
    _init_data()
    yield _db
    _db.drop_all()


@pytest.fixture(scope='module')
def client(app):
    return app.test_client()


@pytest.yield_fixture(scope='function')
def session(db):
    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection)
    session = db.create_scoped_session(options=options)

    db.session = session

    yield session

    transaction.rollback()
    connection.close()
    session.remove()
