import json
from commands.init_data import fb_feed

CONTENT_TYPE = 'application/json'
ERROR_ID = 98239823
NEW_FEED = {
    'title': 'Shinbun.co Hacking News',
    'feed_url': 'https://blog.shinbun.co/feed',
    'site_url': 'https://shinbun.co'
}


def test_get_all_feeds(client, session):
    """
    Tests GET https://api.shinbun.co/feeds/
    """
    resp = client.get('/feeds/')
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 200
    assert isinstance(body, dict)
    assert isinstance(data, list)
    assert isinstance(data[0], dict)


def test_get_one_feed(client, session):
    """
    Tests GET https://api.shinbun.co/feeds/<id>
    """
    resp = client.get('/feeds/1')
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 200
    assert isinstance(data, dict)
    assert data['id'] == 1


def test_get_one_feed_error(client, session):
    """
    Tests GET https://api.shinbun.co/feeds/<id>
    Where <id> is not in the database
    """
    resp = client.get('/feeds/{}'.format(ERROR_ID))
    body = json.loads(resp.data.decode())

    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']


def test_feed_get_articles(client, session):
    """
    Tests GET https://api.shinbun.co/feeds/<id>/articles
    """
    resp = client.get('/feeds/')
    body = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert isinstance(body['data'], list)


def test_feed_get_articles_not_found(client, session):
    """
    Tests GET https://api.shinbun.co/feeds/<id>/articles
    Where <id> is not in the database
    """
    resp = client.get('/feeds/{}/articles'.format(ERROR_ID))
    body = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']


def test_post_new_feed(client, session):
    """
    Tests POST https://api.shinbun.co/feeds/<id>
    """
    resp = client.post('/feeds/',
                       data=json.dumps(NEW_FEED),
                       content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 201
    assert 'location' in resp.headers
    assert '/feeds/{}'.format(body['data']['id']) in resp.headers['location']

    # check that NEW_FEED is a subset of body['data']
    assert NEW_FEED.items() <= body['data'].items()


def test_post_new_feed_missing_fields(client, session):
    """
    Tests POST https://api.shinbun.co/feeds/<id>
    With missing fields
    """
    resp = client.post('/feeds/',
                       data=json.dumps({'title': 'Missing fields feed'}),
                       content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 400
    assert 'missing fields' in body['meta']['reason']
    assert any(key in body['message'] for key in ['site_url', 'feed_url'])


def test_post_new_feed_unique_title(client, session):
    """
    Tests POST https://api.shinbun.co/feeds/<id>
    With an already existing feed_url
    """
    resp = client.post('/feeds/',
                       data=json.dumps(fb_feed),
                       content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 409
    assert 'conflict' in body['meta']['reason']
    assert any(key in body['message'] for key in fb_feed.keys())


def test_update_feed(client, session):
    """
    Tests PUT https://api.shinbun.co/feeds/<id>
    """
    new_data = {'title': "My Awesome Newly Updated Feed Title"}
    resp = client.put('/feeds/1',
                      data=json.dumps(new_data),
                      content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 200
    assert data['title'] == new_data['title']


def test_update_feed_error(client, session):
    """
    Tests PUT https://api.shinbun.co/feeds/<id>
    Where <id> is not in the database
    """
    new_data = {'title': "My Awesome Newly Updated Feed Title"}
    resp = client.put('/feeds/{}'.format(ERROR_ID),
                      data=json.dumps(new_data),
                      content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']


def test_update_feed_unique_constraint(client, session):
    """
    Tests PUT https://api.shinbun.co/feeds/<id>
    With an already existing title.
    """
    new_data = {'title': fb_feed['title']}
    resp = client.put('/feeds/1',
                      data=json.dumps(new_data),
                      content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 409
    assert 'conflict' in body['meta']['reason']
    assert 'title' in body['message']


def test_delete_feed(client, session):
    """
    Tests DELETE https://api.shinbun.co/feeds/<id>
    """
    resp = client.delete('/feeds/1')
    assert resp.status_code == 204


def test_delete_feed_error(client, session):
    """
    Tests DELETE https://api.shinbun.co/feeds/<id>
    Where <id> is not in the database
    """
    resp = client.delete('/feeds/{}'.format(ERROR_ID))
    body = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']
