import feedparser
import pytest
from unittest.mock import patch
from shinbun.parser.parser import parse_url, get_source, get_articles


MOCK_FEED = feedparser.FeedParserDict({
    "feed": {
        "title": "My title",
        "link": "hello.com/feed",
        "icon": "link.to.facivon",
        "language": "en",
        "updated": "2018-12-04T03:48:08-05:00"
    },
    "entries": [
        {
            "title": "my awesome article",
            "published": "2019-11-20",
            "link": "theverge.com/my-awesome-article",
            "author": "agent 47",
            "summary": "cool story brah"
        },
        {
            "title": "new apple product",
            "published": "2019-01-13",
            "link": "techcrunch.com/new-apple-product",
            "author": "Hatem Ben Arfa",
            "summary": "blablabla goal blablab"
        },
        {
            "title": "Bose quiet comfort review",
            "published": "2013-04-30",
            "link": "hello.com",
            "author": "karim benzema",
            "summary": "lorem ipsum dolor les algeriens"
        },
    ]
})


@patch('feedparser.parse')
def test_parse_url(mocked_parser):
    feed_url = "http://hello.com/feed.rss"
    assert mocked_parser is feedparser.parse
    parse_url(feed_url)
    mocked_parser.assert_called_with(feed_url)


def test_failing_parse_url():
    with pytest.raises(Exception) as exc:
        parse_url("hello.com")
    assert "An error occured" in str(exc.value)


@patch('feedparser.parse')
def test_bad_request_parse_url(mocked_parser):
    mocked_parser.return_value.status = 400
    with pytest.raises(Exception) as exc:
        parse_url("hello.com")
    assert "Feed returned a status of 400" in str(exc.value)


@patch('feedparser.parse')
def test_failing_parse_not_found(mocked_parser):
    mocked_parser.return_value.status = 404
    with pytest.raises(Exception) as exc:
        parse_url("https://www.theverge.com/google/ss/index.xml")
    assert "Feed not found." in str(exc.value)


@patch('feedparser.parse')
def test_get_source(mocked_parser):
    mocked_parser.return_value = MOCK_FEED
    source = get_source(mocked_parser.return_value)
    assert source['title'] == mocked_parser.return_value.feed['title']
    assert source['source_url'] == mocked_parser.return_value.feed['link']
    assert source['favicon_url'] == mocked_parser.return_value.feed['icon']
    assert 'language' not in source
    assert 'updated' not in source


@patch('feedparser.parse')
def test_get_articles(mocked_parser):
    mocked_parser.return_value = MOCK_FEED
    articles = get_articles(mocked_parser.return_value)
    assert isinstance(articles, list)
    assert 'title' in articles[0]
    assert 'published_date' in articles[0]
    assert 'url' in articles[0]
    assert 'author' not in articles[0]
    assert 'summary' not in articles[0]
