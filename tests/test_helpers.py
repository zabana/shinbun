import utils.helpers as helpers

"""
Note: command, group and _blueprints are all fixtures
defined under tests/conftest.py.

This module contains test functions that assert the behavior of the helper
functions defined in utils/helpers.py.
"""


def test_is_cli_command(command):
    assert helpers._is_cli_command(command)
    assert not helpers._is_cli_command(object())


def test_is_cli_group(group):
    assert helpers._is_cli_group(group)
    assert not helpers._is_cli_group(object())


def test_is_cli_command_or_group(command, group):
    assert helpers._is_click_command_or_group(command)
    assert helpers._is_click_command_or_group(group)
    assert not helpers._is_click_command_or_group(object())
    assert not helpers._is_click_command_or_group(object())


def test_is_blueprint(_blueprint):
    assert helpers._is_blueprint(_blueprint)
