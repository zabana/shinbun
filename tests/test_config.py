def test_testing_config(app):
    assert app.config['ENV'] == 'testing'
    assert app.config['DEBUG']
    assert app.config['TESTING']


def test_dev_config(app):
    app.config.from_object('shinbun.config.Config')
    assert app.config['ENV'] == 'development'
    assert app.config['DEBUG']
    assert not app.config['TESTING']


def test_production_config(app):
    app.config.from_object('shinbun.config.ProductionConfig')
    assert app.config['ENV'] == 'production'
    assert not app.config['DEBUG']
    assert not app.config['TESTING']
