import json

CONTENT_TYPE = 'application/json'
ERROR_ID = 9080324

NEW_ARTICLE = {
    'title': 'Shinbun.co Hacking News',
    'rss_feed_url': 'https://blog.shinbun.co/feed',
    'guid': 'wjiewlekw',
    'date_published': '01-01-2001',
    'feed_id': 1
}


def test_get_all_articles(client, session):
    """
    Tests GET https://api.shinbun.co/feeds/
    """
    resp = client.get('/articles')
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 200
    assert isinstance(body, dict)
    assert isinstance(data, list)
    assert isinstance(data[0], dict)


def test_get_one_article(client, session):
    """
    Tests GET https://api.shinbun.co/articles/<id>
    """
    resp = client.get('/articles/1')
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 200
    assert isinstance(data, dict)
    assert data['id'] == 1


def test_get_one_article_error(client, session):
    """
    Tests GET https://api.shinbun.co/articles/<id>
    Where <id> is not in the database
    """
    resp = client.get('/articles/{}'.format(ERROR_ID))
    body = json.loads(resp.data.decode())

    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']


def test_create_article(client, session):
    """
    Tests POST https://api.shinbun.co/articles/
    """
    resp = client.post('/articles/',
                       data=json.dumps(NEW_ARTICLE),
                       content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 201
    assert 'location' in resp.headers
    assert '/articles/{}'.format(data['id']) in resp.headers['location']

    # check that NEW_ARTICLE is a subset of body['data']
    assert NEW_ARTICLE.keys() <= data.keys()


def test_create_article_unique_constraint_error(client, session):
    """
    Tests POST https://api.shinbun.co/articles/
    With an already existing article
    """
    resp = client.post('/articles/',
                       data=json.dumps(NEW_ARTICLE),
                       content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())
    assert resp.status_code == 409
    assert 'conflict' in body['meta']['reason']


def test_create_article_missing_fields_error(client, session):
    """
    Tests POST https://api.shinbun.co/articles/<id>
    With missing fields
    """
    resp = client.post('/articles/',
                       data=json.dumps({'title': 'Missing fields feed'}),
                       content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 400
    assert 'missing fields' in body['meta']['reason']


def test_update_article(client, session):
    """
    Tests PUT https://api.shinbun.co/articles/<id>
    """
    new_data = {'title': "My Awesome Newly Updated Article Title"}
    resp = client.put('/articles/1',
                      data=json.dumps(new_data),
                      content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())
    data = body['data']

    assert resp.status_code == 200
    assert data['title'] == new_data['title']


def test_update_article_not_found(client, session):
    """
    Tests PUT https://api.shinbun.co/articles/<id>
    Where <id> is not in the database
    """
    new_data = {'title': "My Awesome Newly Updated Article Title"}
    resp = client.put('/feeds/{}'.format(ERROR_ID),
                      data=json.dumps(new_data),
                      content_type=CONTENT_TYPE)
    body = json.loads(resp.data.decode())

    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']


def test_delete_article(client, session):
    """
    Tests DELETE https://api.shinbun.co/articles/<id>
    """
    resp = client.delete('/articles/1')
    assert resp.status_code == 204


def test_delete_article_not_found(client, session):
    """
    Tests DELETE https://api.shinbun.co/articles/<id>
    Where <id> is not in the database
    """
    resp = client.delete('/articles/{}'.format(ERROR_ID))
    body = json.loads(resp.data.decode())
    assert resp.status_code == 404
    assert str(ERROR_ID) in body['message']
