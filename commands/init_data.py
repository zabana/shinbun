import sys
import click
from flask.cli import with_appcontext
from shinbun.models.feed import Feed
from shinbun.models.article import Article
from shinbun import db
from .reset_db import _reset_db

google_feed = {
    'title': 'Google Feed',
    'feed_url': 'https://google.com',
    'site_url': 'https://google.com/feeds'
}

fb_feed = {
    'title': 'Facebook Feed',
    'feed_url': 'https://fb.com',
    'site_url': 'https://fb.com/feeds'
}

twitter_feed = {
    'title': 'twitter Feed',
    'feed_url': 'https://twitter.com',
    'site_url': 'https://twitter.com/feeds'
}


@click.command('init-data')
@with_appcontext
def init_data():
    _init_data()


def _init_data():
    click.echo("Resetting Database ...")
    _reset_db()
    click.echo("Done !")

    click.echo("Initializing data ...")
    click.echo("Creating feeds ...")

    _create_feeds()

    click.echo("Feeds created successfully !")

    click.echo("Creating articles ...")

    _create_articles()

    click.echo("Articles created successfully !")
    click.echo("Data Initialized !")


def _create_feeds(feeds=[google_feed, fb_feed, twitter_feed]):

    for feed in feeds:
        db.session.add(Feed(**feed))

    try:
        db.session.commit()
    except Exception as e:
        sys.exit("ERROR: {}".format(str(e.__cause__).split('\n')[0]))


def _create_articles():

    feeds = Feed.get_all()
    if len(feeds) < 1:
        sys.exit('There are no feeds to assign articles to. \
                 Create them first.')

    for feed in feeds:
        db.session.add(Article(**_generate_article(feed_id=feed['id'])))
        db.session.add(Article(**_generate_article(feed_id=feed['id'])))

    try:
        db.session.commit()
    except Exception as e:
        sys.exit("ERROR: {}".format(str(e.__cause__).split('\n')[0]))


def _generate_article(feed_id):
    random_id = _generate_random_id()
    return {
        'title': 'article {}_{}'.format(feed_id, random_id),
        'rss_feed_url': 'article{}.com'.format(feed_id),
        'guid': 'article{}_{}'.format(feed_id, random_id),
        'date_published': '01-0{}-2018'.format(feed_id),
        'feed_id': feed_id
    }


def _generate_random_id(size=12):
    import string
    import random

    chars = string.ascii_lowercase + string.ascii_uppercase + string.digits
    return ''.join(random.choice(chars) for _ in range(size))
