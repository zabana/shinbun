import click
from flask.cli import with_appcontext


@click.command('lint')
@with_appcontext
def lint():
    from subprocess import run
    exclude_dirs = [
        'ENV*/',
        '__pycache__/',
        'migrations/'
    ]
    click.echo("Linting project code ...")
    run(['flake8', '--exclude={}'.format(','.join(exclude_dirs))])
    click.echo("No errors found !")
