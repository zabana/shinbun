from importlib import import_module
import click
import inspect
import flask


def register_blueprints(app):
    blueprints = get_blueprints()
    for blueprint in blueprints:
        url_prefix = (blueprint.url_prefix or '').rstrip('/')
        app.register_blueprint(blueprint, url_prefix=url_prefix)


def register_cli_commands(app):
    cli_commands = get_commands()
    for name, command in cli_commands:
        app.cli.add_command(command)


def _is_cli_command(obj):
    return isinstance(obj, click.Command)


def _is_cli_group(obj):
    return isinstance(obj, click.Group) or isinstance(obj, flask.cli.AppGroup)


def _is_click_command_or_group(obj):
    return _is_cli_command(obj) or _is_cli_group(obj)


def _is_blueprint(obj):
    return isinstance(obj, flask.blueprints.Blueprint)


def _get_members(module, predicate):
    for name, obj in inspect.getmembers(module):
        if predicate(obj):
            yield (name, obj)


def get_commands():
    import commands
    group_commands = {}
    for name, group in inspect.getmembers(commands, _is_cli_group):
        group_commands.update(group.commands)
        if name not in commands.EXISTING_CLI_GROUPS:
            yield (name, group)

    yield from _get_members(commands, _is_cli_command)  # noqa


def get_blueprints():
    from shinbun.config import BLUEPRINT_BUNDLES
    for blueprint_module in BLUEPRINT_BUNDLES:
        module = import_module(blueprint_module)

        for name, blueprint in inspect.getmembers(module, _is_blueprint):
            yield blueprint
