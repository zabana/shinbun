# Shinbun

## Project Setup

1. Make sure you have docker and docker compose installed on your system

2. create a .env file with the following variables:

```bash
SHINBUN_DB_USER=db_user
SHINBUN_DB_PASS=db_password
SHINBUN_DB_NAME=db_name
```

## Build and run the project

You can run the server by building the code using docker-compose

```bash
sudo docker-compose up --build -d
```

To access the logs

```bash
docker-compose logs
```

To see information about running containers

```bash
docker-compose ps
```

To access the DB

```bash
docker exec -it <name_of_the_image> psql -U <db_user> -d <db_name> -W
# -W will prompt the user for their password
```

In development, use the default docker-compose file which will run the flask
server and allow for instant reload on code change. For production and staging,
you should specify the `docker-compose.prod.yml` file. This config will run an
WSGI server that can be put behind an nginx instance.

```bash
docker-compose -f /path/to/docker-compose.prod.yml <commands and flags>
```
## Troubleshooting

If you get a `ROLE <role> does not exist` error just dump the data and restart
the containers like so:

```bash
sudo rm -rf postgres-data
docker-compose restart
```
